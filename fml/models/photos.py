from django.db import models

from .positive_big_auto_field import PositiveBigAutoField


class Photos(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    original_photo = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    cached_main_photo_variant = models.ForeignKey('PhotosVariants', models.DO_NOTHING, blank=True, null=True)
    calced_current_price_value = models.PositiveIntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_level = models.PositiveIntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photos'
