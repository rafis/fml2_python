from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .domains import Domains


class DomainsPages(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    domain = models.ForeignKey(Domains, models.DO_NOTHING)
    path = models.TextField()
    prefix = models.CharField(max_length=255)
    reverse_path = models.CharField(max_length=759)
    value = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'domains_pages'
        unique_together = (('domain', 'reverse_path'),)
