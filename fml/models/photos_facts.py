from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos import Photos


class PhotosFacts(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo = models.ForeignKey(Photos, models.DO_NOTHING)
    key = models.CharField(max_length=255)
    value = models.TextField()
    accuracy = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photos_facts'
