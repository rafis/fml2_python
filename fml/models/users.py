from django.db import models
from django.contrib.auth.models import AbstractUser

from .positive_big_auto_field import PositiveBigAutoField


class Users(AbstractUser):
    first_name = None
    last_name = None

    id = PositiveBigAutoField(primary_key=True)
    role = models.CharField(max_length=9)
    name = models.CharField(max_length=255)
    username = models.CharField(unique=True, max_length=150)
    password = models.CharField(max_length=255)
    remember_token = models.CharField(max_length=100, blank=True, null=True)
    objectivity = models.IntegerField(default=0)
    objectivity_updated_at = models.DateTimeField(default='1970-01-01T00:00:01')
    accuracy = models.IntegerField(default=0)
    gp_amount = models.BigIntegerField(default=0)
    experience_value = models.BigIntegerField(default=0)

    class Meta(AbstractUser.Meta):
        managed = False
        db_table = 'users'

#    def save(self, *args, **kwargs):
#        if self.pk is None:
#            assert self.objectivity is None
#            self.objectivity = 0
#            from datetime import datetime
#            self.objectivity_updated_at = datetime.now()
#            self.accuracy = 0
#            self.gp_amount = 0
#            self.experience_value = 0
#        return super().save(*args, **kwargs)
