from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos_variants import PhotosVariants
from .photos_variants_filenames import PhotosVariantsFilenames
from .photos_variants_storagewwws import PhotosVariantsStoragewwws
from .users import Users


class PhotosVariantsSources(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo_variant = models.ForeignKey(PhotosVariants, models.DO_NOTHING)
    source = models.CharField(max_length=6)
    photo_variant_storagewww = models.ForeignKey(PhotosVariantsStoragewwws, models.DO_NOTHING, blank=True, null=True)
    photo_variant_filename = models.ForeignKey(PhotosVariantsFilenames, models.DO_NOTHING, blank=True, null=True)
    uploader_user = models.ForeignKey(Users, models.DO_NOTHING)
    uploader_is_visible = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photos_variants_sources'
