from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos_variants import PhotosVariants
from .domains_pages import DomainsPages


class PhotosVariantsStoragewwws(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo_variant = models.ForeignKey(PhotosVariants, models.DO_NOTHING)
    url_domain_page = models.ForeignKey(DomainsPages, models.DO_NOTHING)
    value = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photos_variants_storagewwws'
        unique_together = (('photo_variant', 'url_domain_page'),)
