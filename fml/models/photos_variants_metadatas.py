from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos_variants import PhotosVariants


class PhotosVariantsMetadatas(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo_variant = models.ForeignKey(PhotosVariants, models.DO_NOTHING)
    type = models.CharField(max_length=7)
    data = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photos_variants_metadatas'
