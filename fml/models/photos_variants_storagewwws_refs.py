from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos_variants import PhotosVariants


class PhotosVariantsStoragewwwsRefs(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo_variant_storagewww = models.ForeignKey(PhotosVariants, models.DO_NOTHING)
    refurl_domain_page_id = models.BigIntegerField(blank=True, null=True)
    value = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photos_variants_storagewwws_refs'
        unique_together = (('photo_variant_storagewww', 'refurl_domain_page_id'),)
