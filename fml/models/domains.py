from django.db import models

from .positive_big_auto_field import PositiveBigAutoField


class Domains(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    host = models.CharField(max_length=255)
    original_domain = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    value = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'domains'
