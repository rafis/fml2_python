from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photosets import Photosets
from .photos import Photos


class PhotosetsPhotos(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photoset = models.ForeignKey(Photosets, models.DO_NOTHING)
    photo = models.ForeignKey(Photos, models.DO_NOTHING)
    user_weight = models.IntegerField()
    system_weight = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photosets_photos'
        unique_together = (('photoset', 'photo'),)
