from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos_variants import PhotosVariants


class PhotosVariantsStoragebackups(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo_variant = models.ForeignKey(PhotosVariants, models.DO_NOTHING, unique=True)

    class Meta:
        managed = False
        db_table = 'photos_variants_storagebackups'
