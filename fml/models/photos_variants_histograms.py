from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos_variants import PhotosVariants


class PhotosVariantsHistograms(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo_variant = models.ForeignKey(PhotosVariants, models.DO_NOTHING, unique=True)
    h0 = models.IntegerField()
    h1 = models.IntegerField()
    h2 = models.IntegerField()
    h3 = models.IntegerField()
    h4 = models.IntegerField()
    h5 = models.IntegerField()
    h6 = models.IntegerField()
    h7 = models.IntegerField()
    h8 = models.IntegerField()
    h9 = models.IntegerField()
    h10 = models.IntegerField()
    h11 = models.IntegerField()
    hsv = models.TextField()
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'photos_variants_histograms'
