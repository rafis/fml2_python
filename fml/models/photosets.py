from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos import Photos
from .users import Users


class Photosets(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    user_id = models.ForeignKey(Users, models.DO_NOTHING)
    type = models.PositiveIntegerField()
    user_search_priority = models.IntegerField()
    system_search_priority = models.IntegerField()
    publicity = models.IntegerField()
    user_given_name = models.CharField(max_length=251)
    user_given_description = models.TextField(blank=True, null=True)
    password = models.CharField(max_length=60, blank=True, null=True)
    passhint = models.CharField(max_length=255, blank=True, null=True)
    cached_main_photo = models.ForeignKey(Photos, models.DO_NOTHING, blank=True, null=True)
    cached_name = models.CharField(max_length=1024, blank=True, null=True)
    cached_description = models.TextField(blank=True, null=True)
    calced_current_price_value = models.PositiveIntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    search_requested_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photosets'
        unique_together = (('user_id', 'user_given_name'),)
