from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos import Photos


class PhotosVariants(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo = models.ForeignKey(Photos, models.DO_NOTHING)
    mimetype = models.PositiveSmallIntegerField()
    filesize = models.PositiveIntegerField()
    sha1 = models.CharField(max_length=20)
    stored_locally = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'photos_variants'
        unique_together = (('mimetype', 'filesize', 'sha1'),)
