from django.db import models

from .positive_big_auto_field import PositiveBigAutoField
from .photos import Photos


class PhotosSearchengines(models.Model):
    id = PositiveBigAutoField(primary_key=True)
    photo = models.ForeignKey(Photos, models.DO_NOTHING)
    searchengine = models.CharField(max_length=6)
    created_at = models.DateTimeField()
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photos_searchengines'
