from django.contrib import admin

from .models import (
    Databasechangelog,
    Databasechangeloglock,
    Domains,
    DomainsPages,
    Photos,
    PhotosFacts,
    PhotosSearchengines,
    PhotosVariants,
    PhotosVariantsFilenames,
    PhotosVariantsHistograms,
    PhotosVariantsMetadatas,
    PhotosVariantsSources,
    PhotosVariantsStoragebackups,
    PhotosVariantsStorages3S,
    PhotosVariantsStoragewwws,
    PhotosVariantsStoragewwwsRefs,
    Photosets,
    PhotosetsPhotos,
    Users,
)

# Register your models here.
admin.site.register(Databasechangelog)
admin.site.register(Databasechangeloglock)
admin.site.register(Domains)
admin.site.register(DomainsPages)
admin.site.register(Photos)
admin.site.register(PhotosFacts)
admin.site.register(PhotosSearchengines)
admin.site.register(PhotosVariants)
admin.site.register(PhotosVariantsFilenames)
admin.site.register(PhotosVariantsHistograms)
admin.site.register(PhotosVariantsMetadatas)
admin.site.register(PhotosVariantsSources)
admin.site.register(PhotosVariantsStoragebackups)
admin.site.register(PhotosVariantsStorages3S)
admin.site.register(PhotosVariantsStoragewwws)
admin.site.register(PhotosVariantsStoragewwwsRefs)
admin.site.register(Photosets)
admin.site.register(PhotosetsPhotos)
admin.site.register(Users)
