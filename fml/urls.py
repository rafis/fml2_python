from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('auth/login/', auth_views.LoginView.as_view(template_name='auth/login.html'), name='auth/login'),
    path('auth/logout/', auth_views.LogoutView.as_view(template_name='auth/logged_out.html'), name='auth/logout'),
    path('auth/signup/', views.auth.signup, name='auth/signup'),
    path('', views.index, name='index'),
]
