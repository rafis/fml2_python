from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
 
from ..models import Users
 
 
class SignUpForm(UserCreationForm):
#    full_name = forms.CharField(max_length=100, help_text='Required. 100 charaters of fewer.')
 
    class Meta:
        model = Users
        fields = UserCreationForm.Meta.fields + ('name',)
